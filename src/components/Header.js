import React, { Component } from "react";
import { withRouter } from 'react-router-dom'
import { NavbarNav, NavItem, NavLink, Navbar, FormInline, Button} from 'mdbreact';


class Header extends Component {

  constructor(props) {
    super(props);

    this.handleClick = this.handleClick.bind(this);
  }


  handleClick(e) {
    // e.preventDefault();
    let data = document.getElementById("form").value;
    this.props.history.push(`/search/${data}`);
  }



 render () {
   return (
       <Navbar color="blue-grey lighten-2" dark expand="sm" scrolling fixed="top">
           <NavLink to='/' className="inactive" activeClassName="active" exact={true}>Home</NavLink>
           <NavbarNav right>
               <NavItem onClick={() => this.props.history.push('/businesses')}>
                  <NavLink to=''>Businesses</NavLink>
                </NavItem>
               <NavItem onClick={() => this.props.history.push('/industries')}>
                  <NavLink to=''>Industries</NavLink>
               </NavItem>
               <NavItem onClick={() => this.props.history.push('/states')}>
                  <NavLink to=''>States</NavLink>
                </NavItem>
               <NavItem onClick={() => this.props.history.push('/about')}>
                  <NavLink to=''>About</NavLink>
               </NavItem>
               <NavItem>
               <FormInline className="md-form active-purple-2" style={{margin:'10px 5px', maxHeight:'30px', padding:'0 10px'}}>
                <input style={{padding:'0'}} className="form-control form-control-sm mr-3 w-75" type="text" id="form" onSubmit={e => (e.preventDefault())} placeholder="Search" aria-label="Search"/>
                <Button outline color="white" style={{display:'none'}} size="sm" type="submit" className="mr-auto" onClick={this.handleClick}>Search</Button>
              </FormInline>
                </NavItem>
           </NavbarNav>
       </Navbar>
   )
 }

}

export default withRouter(Header);
