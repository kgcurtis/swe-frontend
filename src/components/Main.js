import React, { Component } from "react";
import { Switch, Route } from 'react-router-dom'
import Home from './Home'
import Businesses from './Business/Businesses'
import Industries from './Industry/Industries'
import States from './State/States'
import About from './About'
import SearchResults from './SearchResults'
import { Container } from 'mdbreact';
import {withRouter } from 'react-router-dom';

class Main extends Component {
  render() {
    return (
      <Container>
        <main>
          <Switch>
            <Route exact path='/' component={Home}/>
            <Route path='/businesses' component={Businesses}/>
            <Route path='/industries' component={Industries}/>
            <Route path='/states' component={States}/>
            <Route path='/about' component={About}/>
            <Route path='/search/:term' component={SearchResults}/>
          </Switch>
        </main>
        </Container>
    )
  }
}

export default withRouter(Main);
