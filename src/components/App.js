import React, { Component } from "react";
import Header from './Header'
import Main from './Main'

class App extends Component {
  render () {
    return (
      <div>
       <Header history={this.props.history}/>
       <Main history={this.props.history}/>
     </div>
    )
  }
}

export default App
