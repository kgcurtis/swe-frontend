import React, { Component } from "react";
import TeamAPI from '../apis/teamAPI'
import { Container, Button, Row, Col, Card, CardText, CardBody, CardTitle, CardImage,
Table, TableBody, TableHead, ListGroup, ListGroupItem } from 'mdbreact';
import classnames from 'classnames';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';


class About extends Component {
  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);
    this.state = {
      members: [],
      commits_be: [],
      commits_fe: [],
      issues_be: [],
      issues_fe: [],
      commits_total: 0,
      issues_total: 0,
      tests_total: 0,
      activeItem: '0'
    };
  }

  toggle(tab) {
    if (this.state.activeItem !== tab) {
      this.setState({
        activeItem: tab
      });
    }
  }

  componentDidMount() {
    TeamAPI.all().then(results =>
      {
        this.setState({ commits_be: results[0], commits_fe: results[1], issues_be: results[2],
                        issues_fe: results[3]})
        var m = [
        { id: '1', name: "Ben Chen", handle: "wenkaichen", username:"ben12143chen", role: "Front end dev", commits: 0, issues: 0, bio:"Ben is a computer science senior. He likes dogs.", img: "/profiles/ben.jpeg", tests: 15},
        { id: '2', name: "Ryan Rice", handle: "ryanr1230", username:"ryanr1230", role: "Backend and API dev", commits: 0, issues: 0, bio: "I'm a senior studying Computer Science that likes board games and competitive programming.", img: "/profiles/ryan.jpg", tests: 30},
        { id: '3', name: "Katherine Curtis", handle: "kcurtis26518", username:"kgcurtis", role: "Front end dev", commits: 0, issues: 0, bio: "I am a senior majoring in computer science, with certificates in business and Chinese language.", img: "/profiles/katherine.jpeg", tests: 18},
        { id: '4', name: "Shuai Xu", handle: "shuaixu", username:"shuaixu1997", role: "Back end & Server dev", commits: 0, issues: 0, bio: "CS senior struggling for a job. He likes recording and audio mixing stuff.", img: "/profiles/shuai.jpg", tests: 6},
        { id: '5', name: "Edgars Vitolins", handle: "vitolins.edgars", username:"vitolins.edgars", role: "Frontend and API dev", commits: 0, issues: 0, bio: "I am a UT Computer Science senior from Latvia.", img: "/profiles/edgars.jpg", tests :10}
        ]
        var commits = {}
        var com_total = 0
        var issues = {}
        var iss_total = 0

        var tes_total = 0

        var com = this.state.commits_be
        com = com.concat(this.state.commits_fe)

        var iss = this.state.issues_be
        iss = iss.concat(this.state.issues_fe)

        for (var i in com) {
          var email = com[i].committer_email;
          var name = email.substring(0, email.indexOf("@"))
          if (name in commits) {
            commits[name] += 1;
          } else {
            commits[name] = 1;
          }
        }

        for (var j in iss) {
          var username = iss[j].author["username"];
          if (username in issues) {
            issues[username] += 1;
          } else {
            issues[username] = 1;
          }
        }

        commits["shuaixu"] += commits["root"]
        commits["vitolins.edgars"] += commits["edgarsv"]

        for (var n in m) {
            if (m[n].handle in commits) {
              m[n].commits = commits[m[n].handle]
              com_total += commits[m[n].handle]
            }
            if (m[n].username in issues) {
              m[n].issues = issues[m[n].username]
              iss_total += issues[m[n].username]
            }
            tes_total += m[n].tests
        }
        this.setState({members: m, commits_total: com_total, issues_total: iss_total,
                       tests_total: tes_total})
      })

  }

  render() {
    const membersTabs = this.state.members.map(member => (
      <Tab key={member.id}>
        <Button
          className={classnames({ active: this.state.activeItem === member.id })}
          onClick={() => { this.toggle(member.id); }}>
          {member.name}
        </Button>
      </Tab>
    ));
    const members = this.state.members.map(member => (
      <TabPanel tabId={member.id} key={member.id}>
        <Row>
          <Col sm="4" key={member.id}>
                <br />
          <Card style={{margin: "0 0 20px 0"}} cascade>
            <CardImage cascade className="img-fluid" src={member.img} />
            <CardBody cascade>
                <CardTitle>{member.name}</CardTitle>
                <CardText>{member.role}</CardText>
            </CardBody>
            <div className="rounded-bottom mdb-color lighten-3 text-center pt-3">
                <div className="font-small" style={{margin: "0px 10px 10px 10px"}}>
                    <div className="pr-2 white-text">Bio: {member.bio}</div>
                    <div className="pr-2 white-text">Commits: {member.commits}</div>
                    <div className="pr-2 white-text">Issues: {member.issues}</div>
                    <div className="pr-2 white-text">Unit Tests: {member.tests}</div>
                </div>
            </div>
          </Card>
        </Col>
      </Row>
    </TabPanel>
      ));
  return (
      <div>
        <Container>
  <h1 className="title" style={{fontSize:"50px", padding:"0 0 30px"}}>About Us</h1>
        <p className="text-justify"><b>The purpose</b> of Ethical Businesses is to allow individuals to make informed decisions on ethical companies, and industries. An ethical business assessment is made by compiling business ratings from the Better Business Bureau (BBB) and B-corporation assessment scores. The BBB score takes into account historical reviews and complaints. The B-corporation score is determined from an assessment taking into account workplace ethics, sustainability, and other metrics. Ethical Businesses combines three related models - individual business profiles, industries, and states. The user will be able to see average scores within a particular industry or state. Businesses are also ranked against other businesses, based on aggregated score and rating data.
explanation of the interesting result of integrating disparate data</p>

        <p className="text-justify">By <b>integrating disparate data</b> we were able to gain more insight into what ethical and not so ethical businesses look like. For example, looking at specific industries one can observe that some industries have singnificantly fewer ethical businesses than others. Professional Scientific and Technical Services industry has 21 firms, while Finance and Insurance has only 4.</p>
        <Row>
          <Col>
          <h2>Click to see information about each team member</h2>
          <Tabs style={{padding:'0 100px'}}>
              <TabList>
              {membersTabs}
              </TabList>
              <br /><br /><br /><br />
              {members}
            </Tabs>
              <Table className="stats-table">
                <TableHead>
                  <tr>
                    <th>name</th>
                    <th>number</th>
                  </tr>
                </TableHead>
                <TableBody className="stats-body">
                  <tr>
                    <td>Commits</td>
                    <td>{this.state.commits_total}</td>
                  </tr>
                  <tr>
                    <td>Issues</td>
                    <td>{this.state.issues_total}</td>
                  </tr>
                  <tr>
                    <td>Unit Tests</td>
                    <td>{this.state.tests_total}</td>
                  </tr>
                </TableBody>
              </Table>
            </Col>

        </Row>


        <h3 style={{fontSize:"30px", padding:"50px 0 0 0"}}>Tools</h3>
        <ListGroup className="tools">
          <ListGroupItem>
            <div className="d-flex w-100 justify-content-between">
              <h5 className="mb-1">Bootstrap</h5>
            </div>
            <p className="mb-1">Twitter Bootstrap is a CSS framework that provides an abundance of class labels and IDs that come preloaded with responsive CSS attributes to any screen size (including mobile devices!) and other formatting issues. Along with responsive design, it provides common website features such as a floating navigation bar and a background splash page, which along with open source templates leads to very easy and very quick creation of a professional looking page. </p>
          </ListGroupItem>
          <ListGroupItem>
            <div className="d-flex w-100 justify-content-between">
              <h5 className="mb-1">Postman</h5>
            </div>
            <p className="mb-1">Postman was a useful tool in creating the initial architecture of our API. We were able to use the interface that both let us view how to create requests with our current design and at the same time make changes that were instantly reflected in the documentation of our RESTful API. It also allows for easy organization within subfolders and produces actual code snippets that could be useful for testing in future phases. It also provides a place for example responses for prospective users so you know exactly what fields our JSON formatted data will be coming in. </p>
          </ListGroupItem>
          <ListGroupItem>
            <div className="d-flex w-100 justify-content-between">
              <h5 className="mb-1">GitLab</h5>
            </div>
            <p className="mb-1">The codebase is hosted on GitLab at https://gitlab.com/kgcurtis/swe. GitLab is one of numerous free git repository hosting services, similar to Bitbucket and GitHub. Unlike the popular GitHub, GitLab has built-in issue tracking and CI, which simplified our development process.</p>
          </ListGroupItem>
          <ListGroupItem>
            <div className="d-flex w-100 justify-content-between">
              <h5 className="mb-1">Pagination</h5>
            </div>
            <p className="mb-1">Pagination allowed us to both present the data in models page in a less cluttered way as well as reduce the load time of the website by requireing less processing of data. Our API allows to request model instances using pagination by passing in a parameter "page." The Front-end server then uses our API with pagination to request some set of instances at a time when a user navigates the front end pagination controls.</p>
          </ListGroupItem>
          <ListGroupItem>
            <div className="d-flex w-100 justify-content-between">
              <h5 className="mb-1">Testing</h5>
            </div>
            <p className="mb-1">We used Mocha, Selenium, python unittest and Postman tests while developing both the front-end and the back-end servers. We used Mocha and Seelenium for front-end white box and black box testing. For back-end development we used python unittest for white box tests, and the Postman API test tool for our back-end acceptance tests.</p>
          </ListGroupItem>
          <ListGroupItem>
            <div className="d-flex w-100 justify-content-between">
              <h5 className="mb-1">Database</h5>
            </div>
            <p className="mb-1">We use SQLite for persistent storage. The database is hosted on our AWS EC2 instance and provides the data that is served by our API. We decided to choose SQLite so that we can easily have a production copy on our local development environment.</p>
          </ListGroupItem>
          <ListGroupItem>
            <div className="d-flex w-100 justify-content-between">
              <h5 className="mb-1">Filtering</h5>
            </div>
            <p className="mb-1">We use filters to reduce the number of instances returned and presented to the user. In our case, we use drop-down buttons to filter the results. Filtered fields will show up as chips right below the drop-down, and the instances will update automatically. Filtering is supported by our Front-end and is done without reloading the page or making calls to the Back-end API.</p>
          </ListGroupItem>
          <ListGroupItem>
            <div className="d-flex w-100 justify-content-between">
              <h5 className="mb-1">Searching</h5>
            </div>
            <p className="mb-1">Navigation bar and model page search-bars allow the user to find specific instances of our business, industry and state models. The search-bar in our navigation bar is implemented with our Back-end API, and require the user to reload the page to return the results. The relevant keywords in the results are highlighted. The model page search-bars are implemented in the Front-end and do not require the user to reload the page to return the results. The results are refreshed after every change to the query.</p>
          </ListGroupItem>
          <ListGroupItem>
            <div className="d-flex w-100 justify-content-between">
              <h5 className="mb-1">Sorting</h5>
            </div>
            <p className="mb-1">A drop down list allows the user to sort the cards on our model pages. This is also implemented in our Front-end and does not require the page to be reloaded in order to return the results. Just like with filtering, a chip below the drop down shows the user what the cards are sorted by. Sorting can be used together with filtering and model page searching.</p>
          </ListGroupItem>
        </ListGroup>

        <h3 style={{fontSize:"30px", padding:"50px 0 0 0"}}>Sources</h3>
        <ListGroup className="sources">
          <ListGroupItem>
            <div className="d-flex w-100 justify-content-between">
              <h5 className="mb-1">Better Business Bureau: <a style={{color: "#212529", padding:"0 0 0 30px"}} href="https://developer.bbb.org/documentation/openapi">https://developer.bbb.org/documentation/openapi</a></h5>
            </div>
            <p className="mb-1">We registered an account at developer.bbb.org to get an authentication token. This token was used to make http requests with python as described in the api documentation.</p>
          </ListGroupItem>
          <ListGroupItem>
            <div className="d-flex w-100 justify-content-between">
              <h5 className="mb-1">Bureau of Economic Analysis: <a style={{color: "#212529", padding:"0 0 0 30px"}} href="https://apps.bea.gov/API/signup/index.cfm">https://apps.bea.gov/API/signup/index.cfm</a></h5>
            </div>
            <p className="mb-1">Similar to BBB, we registered an e-mail address to receive an API key. We used this key in a python script to retrieve the data following the api documentation.</p>
          </ListGroupItem>
          <ListGroupItem>
            <div className="d-flex w-100 justify-content-between">
              <h5 className="mb-1">North American Industry Classification System: <a style={{color: "#212529", padding:"0 0 0 30px"}} href="http://api.naics.us/">http://api.naics.us/</a></h5>
            </div>
            <p className="mb-1">This was the third and last of our data sources that we scraped using a RESTful API. We created a python script that made http requests as described in the documentation. No account registration was needed as there is no authorization token for NAICS.</p>
          </ListGroupItem>
          <ListGroupItem>
            <div className="d-flex w-100 justify-content-between">
              <h5 className="mb-1">data.world: <a style={{color: "#212529", padding:"0 0 0 30px"}} href="https://apidocs.data.world/">https://apidocs.data.world/</a></h5>
            </div>
            <p className="mb-1">We used their website Front-end search bar to look up interesting data sets, and downloaded their csv files. We then imported these files into our database.</p>
          </ListGroupItem>
        </ListGroup>
        <div className="links">
          <p className="text-justify" style={{fontSize:"20px",padding:"50px 0 0 0"}}>
            GitLab Front-End repo: <a style={{color: "#212529", padding:"0 0 0 30px"}} href="https://gitlab.com/kgcurtis/swe-frontend">https://gitlab.com/kgcurtis/swe-frontend</a>
          </p>
          <p className="text-justify" style={{fontSize:"20px"}}>
            GitLab Back-End repo: <a style={{color: "#212529", padding:"0 0 0 30px"}} href="https://gitlab.com/kgcurtis/swe">https://gitlab.com/kgcurtis/swe</a>
          </p>
          <p className="text-justify" style={{fontSize:"20px",padding:"0 0 0 0"}}>
            Postman API: <a style={{color: "#212529", padding:"0 0 0 30px"}} href="http://api.ethicalbusinesses.me/">http://api.ethicalbusinesses.me/</a>
          </p>
          <p className="text-justify" style={{fontSize:"20px",padding:"0 0 0 0"}}>
            <a style={{color: "#212529", padding:"0 0 0 0px"}} href="visualization1.html">Visualization 1</a>
          </p>
          <p className="text-justify" style={{fontSize:"20px",padding:"0 0 0 0"}}>
            <a style={{color: "#212529", padding:"0 0 0 0px"}} href="visualization2.html">Visualization 2</a>
          </p>
          <p className="text-justify" style={{fontSize:"20px",padding:"0 0 50px 0"}}>
            <a style={{color: "#212529", padding:"0 0 0 0px"}} href="visualization3.html">Visualization 3</a>
          </p>
        </div>
        </Container>
      </div>
    )
  }
}

export default About;
