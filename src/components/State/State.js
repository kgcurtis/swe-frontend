import React, { Component } from "react";
import StateAPI from '../../apis/stateAPI'
import { Container, Button, Card, Col, Row, Badge, ListGroup, ListGroupItem } from 'mdbreact';
import MapContainer from '../MapContainer'

class State extends Component {
  constructor(props) {
    super(props);
    this.id = props.match.params.id
    this.state = {
      state: null
    };
  }

  componentDidMount() {
    StateAPI.get(this.id).then(results => this.setState({ state: results }))
  }

render() {

  if (!this.state.state) {
    return <div><Container></Container></div>
  }
  return (
    <div>
      <Container style={{textAlign:"left"}}>
        <Row>
        <Col className="mb-2" sm="7">
          <h2 className="title">{this.state.state.state_name}</h2>
          <div style={{height:'400px'}}>
          <MapContainer lat={this.state.state.latitude} lng={this.state.state.longitude} zoom={6}/>
          </div>
        </Col>
          <Col className="mb-2" sm="5">
            <Card>
            <ListGroup>
              <ListGroupItem>GDP: <Badge style={{float: "right"}} color="info">{this.state.state.gdp}</Badge></ListGroupItem>
              <ListGroupItem># of B-Corps:<Badge style={{float: "right"}} color="info" pill>{this.state.state.num_of_bcorps}</Badge></ListGroupItem>
              <ListGroupItem># of A+ Businesses: <Badge style={{float: "right"}} color="amber darken-2" pill>{this.state.state.num_of_aplus_businesses}</Badge></ListGroupItem>
              <ListGroupItem>Largest Industry: <Badge style={{float: "right"}} color="amber darken-2" pill>{this.state.state.largest_industry_name}</Badge></ListGroupItem>
          </ListGroup>
            </Card>
          </Col>

        </Row>
        <Row>
          <Col className="mt-2" sm="4">
            <Button onClick={() => this.props.history.goBack()}>Go Back</Button>
          </Col>
        </Row>
      </Container>
    </div>
  )
}
}

export default State
