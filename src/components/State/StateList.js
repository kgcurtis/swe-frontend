import React, { Component } from "react";
import StateAPI from '../../apis/stateAPI'
import { Container, Row, Col, Card, CardBody, CardTitle, Button, Pagination, PageLink, PageItem, ListGroup, ListGroupItem, Badge,Dropdown, DropdownToggle, DropdownMenu, DropdownItem   } from 'mdbreact';
import Highlighter from "react-highlight-words";

class StateList extends Component {

  constructor(props) {
    super(props);
    this.state = {
      states: [],
      pageNumber: 1,
      pages: [],
      searchTerm: "",
      startInd: 0,
      endInd: 12,
      sortFunction: function(a, b){return parseFloat(b.gdp) - parseFloat(a.gdp);},
      sortByName: "GDP High-Low",
      filterFunction: function(b){return true; },
      industryFilter: "",
      industryNames: ['Professional Scientific and Technical Services','Administrative and Support and Waste Management and Remediation Services','Transportation and Warehousing','Manufacturing','Construction','Health Care and Social Assistance','Arts Entertainment and Recreation','Accommodation and Food Services','Wholesale Trade','Retail Trade','Information','Finance and Insurance','Educational Services','Other Services (except Public Administration)','Real Estate and Rental and Leasing'],
      globalSearch: this.props.searchTerm === undefined ? "" : this.props.searchTerm
    };

    this.filterList = this.filterList.bind(this);

  }

  componentDidMount() {
    var complete = [];
    if(this.state.globalSearch.length === 0){
    StateAPI.getAll().then(results => {
      this.setState({ states: results, pages: Array.from(new Array(Math.floor(complete.length/12) + 1), (x,i) => i + 1) });
    });
  } else {
    StateAPI.searchAll(this.state.globalSearch).then(results => {
      this.setState({ states: results, pages: Array.from(new Array(Math.floor(complete.length/12) + 1), (x,i) => i + 1) });
    });
  }
}


  getNext() {
    if(this.state.pageNumber < this.state.pages.length - 1){
      this.setState({
            startInd: this.state.startInd + 12,
            endInd: this.state.endInd + 12,
            pageNumber: this.state.pageNumber + 1
      })
    }
  }

  getPrev() {
      if(this.state.pageNumber > 1){
      this.setState({
            startInd: this.state.startInd - 12,
            endInd: this.state.endInd - 12,
            pageNumber: this.state.pageNumber - 1
      })
    }
  }

  getPage(pageNumber) {
    this.setState({
          startInd: (pageNumber - 1) * 12,
          endInd: (pageNumber * 12),
          pageNumber: pageNumber
      })
  }

  filterList(event) {
    this.setState({ searchTerm: event.target.value.toLowerCase() });
  }

  industryFilter(industryName) {
      this.setState({ industryFilter: industryName });
  }

  sortFunction(sortBy){

    switch(sortBy){
      case 1:
        this.setState({ sortByName: "Name A-Z", sortFunction: function(a, b){return a.state_name.localeCompare(b.state_name);} });
        break;
      case 2:
        this.setState({ sortByName: "Name Z-A", sortFunction: function(a, b){return b.state_name.localeCompare(a.state_name);} });
        break;
      case 3:
        this.setState({ sortByName: "GDP High-Low", sortFunction: function(a, b){return parseFloat(b.gdp) - parseFloat(a.gdp);} });
        break;
      case 4:
        this.setState({ sortByName: "GDP Low-High", sortFunction: function(a, b){return parseFloat(a.gdp) - parseFloat(b.gdp);} });
        break;
      case 5:
        this.setState({ sortByName: "# of B-Corps High-Low", sortFunction: function(a, b){return parseFloat(b.num_of_bcorps) - parseFloat(a.num_of_bcorps);} });
        break;
      case 6:
        this.setState({ sortByName: "# of B-Corps Low-High", sortFunction: function(a, b){return parseFloat(a.num_of_bcorps) - parseFloat(b.num_of_bcorps);} });
        break;
      case 7:
        this.setState({ sortByName: "# of A+ High-Low", sortFunction: function(a, b){return parseFloat(b.num_of_aplus_businesses) - parseFloat(a.num_of_aplus_businesses);} });
        break;
      case 8:
        this.setState({ sortByName: "# of A+ Low-High", sortFunction: function(a, b){return parseFloat(a.num_of_aplus_businesses) - parseFloat(b.num_of_aplus_businesses);} });
        break;
      default:
        break;
    }

  }

  render() {
    var regex = new RegExp(this.state.searchTerm.replace(/([()[{*+.$^\\|?])/g, '\\$&'), 'i');
    var industryRegex = new RegExp(this.state.industryFilter, 'i');

     const state = this.state.states
    .filter(state => regex.test(state.state_name) || regex.test(state.state_short_name) || regex.test(state.gdp) || regex.test(state.num_of_bcorps) || regex.test(state.num_of_aplus_businesses) || regex.test(state.largest_industry_name))
    .filter(b => industryRegex.test(b.largest_industry_name))
    .sort(this.state.sortFunction)
    .slice(this.state.startInd,this.state.endInd)
    .map(state => (
            <Col sm="12" md="6" lg="4" key={state.state_short_name}>
              <Card style={{padding: "20px", margin: "0 0 30px 0", minHeight: "300px"}}>
              <CardBody>
                <CardTitle><Highlighter searchWords={this.state.globalSearch.replace(/([()[{*+.$^\\|?])/g, '\\$&').split(" ")} textToHighlight={state.state_name}/></CardTitle>
                <ListGroup>
                  <ListGroupItem>GDP: <Badge style={{float: "right"}} color="info darken-2" pill><Highlighter searchWords={this.state.globalSearch.replace(/([()[{*+.$^\\|?])/g, '\\$&').split(" ")} textToHighlight={state.gdp.toString()}/></Badge></ListGroupItem>
                  <ListGroupItem># of A+ Businesses: <Badge style={{float: "right"}} color="info darken-2" pill><Highlighter searchWords={this.state.globalSearch.replace(/([()[{*+.$^\\|?])/g, '\\$&').split(" ")} textToHighlight={state.num_of_aplus_businesses.toString()}/></Badge></ListGroupItem>
                  <ListGroupItem>Largest Industry: <Badge style={{maxWidth: "90px", float: "right"}} color="info darken-2" pill><Highlighter searchWords={this.state.globalSearch.replace(/([()[{*+.$^\\|?])/g, '\\$&').split(" ")} textToHighlight={state.largest_industry_name}/></Badge></ListGroupItem>

                </ListGroup>
              </CardBody>
              <Button color="info" onClick={() => this.props.history.push(`/states/${state.state_short_name}`)}>
                See More
            </Button>
              </Card>
            </Col>
          ));

          var num = Array.from(new Array(Math.floor(this.state.states
         .filter(state => regex.test(state.state_name))
         .filter(b => industryRegex.test(b.largest_industry_name))
         .sort(this.state.sortFunction).length/12) + 1), (x,i) => i + 1)
          const pages = num.map(i => (
            <PageItem active={this.state.pageNumber === i} key={i}>
                <PageLink className="page-link" onClick={() => this.getPage(i)}>
                   {i}
                </PageLink>
            </PageItem>
          ));

          const industryDropdown = this.state.industryNames.map(ind => (
            <DropdownItem key={ind} onClick={()=> this.industryFilter(ind)}>{ind}</DropdownItem>
          ));

    return (
        <div>
          <Container>
          <div className="header">
            <h1 style={{fontSize:"50px", padding:"0 0 30px"}}>States</h1>
             {this.state.globalSearch.length === 0 && <form>
               <fieldset className="form-group">
               <input type="text" className="form-control form-control-md" placeholder="Search States..." onChange={this.filterList}/>
               </fieldset>
             </form> }
          </div>
          {this.state.globalSearch.length === 0 && <div>
            <Row>
              <Col>
                <div>
                  <Dropdown size="sm" style={{float:'left'}}>
                    <DropdownToggle caret color="primary" className="force-scroll">
                    Sort By
                    </DropdownToggle>
                      <DropdownMenu>
                        <DropdownItem onClick={() => this.sortFunction(1)}><b>Name</b> A-Z</DropdownItem>
                        <DropdownItem onClick={() => this.sortFunction(2)}><b>Name</b> Z-A</DropdownItem>
                        <DropdownItem onClick={() => this.sortFunction(3)}><b>GDP</b> High-Low</DropdownItem>
                        <DropdownItem onClick={() => this.sortFunction(4)}><b>GDP</b> Low-High</DropdownItem>
                        <DropdownItem onClick={() => this.sortFunction(5)}><b># of B-Corps</b> High-Low</DropdownItem>
                        <DropdownItem onClick={() => this.sortFunction(6)}><b># of B-Corps</b> Low-High</DropdownItem>
                        <DropdownItem onClick={() => this.sortFunction(7)}><b># of A+</b> High-Low</DropdownItem>
                        <DropdownItem onClick={() => this.sortFunction(8)}><b># of A+</b> Low-High</DropdownItem>
                      </DropdownMenu>
                  </Dropdown>
                  <Dropdown size="sm" style={{float:'left'}}>
                    <DropdownToggle caret color="secondary">
                    Filter By Largest Industry
                    </DropdownToggle>
                      <DropdownMenu>
                      <DropdownItem onClick={()=> this.industryFilter("")}>None</DropdownItem>
                        { industryDropdown }
                      </DropdownMenu>
                  </Dropdown>
                </div>
              </Col>
            </Row>
          <div>
            <Row>
              <Col>
                <div style={{float:'left', padding:'10px'}}>
                <Badge color="grey" pill style={{margin: '10px', padding:'10px'}}>{this.state.sortByName}</Badge>
                { this.state.industryFilter.length > 0 && <Badge color="grey" pill style={{margin: '10px', padding:'10px'}}>{this.state.industryFilter}</Badge> }
                </div>
              </Col>
            </Row>
          </div>
          </div> }
            <Row>
              { state }
            </Row>
            <Row>
            <Col sm="12">
            <div style={{margin:"auto", width: "275px"}}>
            <Pagination className="pg-teal">
                <PageItem key="previous">
                    <PageLink className="page-link" aria-label="Previous" onClick={() => this.getPrev()}>&laquo;</PageLink>
                </PageItem>
                {pages}
                <PageItem key="next">
                    <PageLink className="page-link" aria-label="Next" onClick={() => this.getNext()}>&raquo;</PageLink>
                </PageItem>
            </Pagination>
            </div>
            </Col>
            </Row>
          </Container>
        </div>
    )
  }
}

export default StateList
