import React from 'react'
import { Switch, Route } from 'react-router-dom'
import StateList from './StateList'
import State from './State'

const States = () => (
  <Switch>
    <Route exact path='/states' component={StateList}/>
    <Route path='/states/:id' component={State}/>
  </Switch>
)


export default States
