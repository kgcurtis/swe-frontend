import React, { Component } from 'react';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import { Container, Button, Row, Col } from 'mdbreact';
import classnames from 'classnames';
import BusinessList from './Business/BusinessList';
import IndustryList from './Industry/IndustryList';
import StateList from './State/StateList';


class SearchResults extends Component {
  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);
    this.state = {
      searchTerm: props.match.params.term,
      activeItem: '0'
    };
  }

  toggle(tab) {
    if (this.state.activeItem !== tab) {
      this.setState({
        activeItem: tab
      });
    }
  }

  render() {
    return (
      <Container>
      <h2>Searching for {this.state.searchTerm}...</h2>
      <Tabs>
          <TabList  style={{padding:'0 320px', width:'100%'}}>
          <Tab>
            <Button className={classnames({ active: this.state.activeItem === 'Businesses' })} onClick={() => { this.toggle('Businesses'); }}>Businesses</Button>
          </Tab>
          <Tab>
            <Button className={classnames({ active: this.state.activeItem === 'Industries' })} onClick={() => { this.toggle('Industries'); }}>Industries</Button>
          </Tab>
          <Tab>
            <Button className={classnames({ active: this.state.activeItem === 'States' })} onClick={() => { this.toggle('States'); }}>States</Button>
          </Tab>
          </TabList>
          <br /><br /><br /><br />
          <TabPanel tabId='Businesses' style={{width:'100%'}}>
            <Row>
              <Col>
                <BusinessList searchTerm={this.state.searchTerm}  history={this.props.history}/>
              </Col>
          </Row>
        </TabPanel>
        <TabPanel tabId='Industries'>
          <Row>
            <Col>
              <IndustryList searchTerm={this.state.searchTerm}  history={this.props.history}/>
            </Col>
        </Row>
      </TabPanel>
      <TabPanel tabId='States'>
        <Row>
          <Col>
            <StateList searchTerm={this.state.searchTerm}  history={this.props.history}/>
          </Col>
      </Row>
    </TabPanel>
        </Tabs>
        </Container>


    );
  }
}

export default SearchResults;
