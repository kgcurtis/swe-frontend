import React, { Component } from "react";
import {Map, Marker, GoogleApiWrapper} from 'google-maps-react';

const mapStyles = {
  width: '600px',
  height: '400px'
};

export class MapContainer extends Component {
  render() {
      return (
        <Map initialCenter={{
           lat: this.props.lat,
           lng: this.props.lng
         }} google={this.props.google} zoom= {this.props.zoom} style={mapStyles}>
          <Marker onClick={this.onMarkerClick}
                  name={'Current location'} />
        </Map>
      );
  }
}

export default GoogleApiWrapper({
  apiKey: ('AIzaSyDx3RZRKS5TQWRz6Ey35WMJwhP_rDOAims')
})(MapContainer)
