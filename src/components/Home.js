import { withRouter } from 'react-router-dom';
import React from 'react';
import  { Carousel, CarouselInner, CarouselItem, View, Container } from 'mdbreact';

const Home = () => (
  <div>
    <Container>
    <h2 style={{padding:"10px 0 30px 10px"}}>Welcome to the Ethical Businesses Website!</h2>
      <Carousel
        activeItem={1}
        length={3}
        showControls={true}
        showIndicators={false}
        className="z-depth-1 home">
        <CarouselInner>
          <CarouselItem itemId="1">
            <View>
              <img className="d-block w-100" src='header.png' style={{margin:'auto auto auto auto'}} alt="Third slide" />
            </View>
          </CarouselItem>
          <CarouselItem itemId="2">
            <View>
              <img className="d-block" style={{height:'90%', width:'90%', margin: 'auto'}} src='home1.jpg' alt="First slide" />
            </View>
          </CarouselItem>
          <CarouselItem itemId="3">
            <View>
              <img className="d-block w-100" src='home2.png' style={{height:'100%'}} alt="Second slide" />
            </View>
          </CarouselItem>
        </CarouselInner>
      </Carousel>
    </Container>
  </div>
)

export default withRouter(Home);
