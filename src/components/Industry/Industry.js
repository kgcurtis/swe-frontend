import React, { Component } from "react";
import IndustryAPI from '../../apis/industryAPI'
import { Container, Button, Card, Col, Row, Badge, ListGroup, ListGroupItem } from 'mdbreact';

class Industry extends Component {
  constructor(props) {
    super(props);
    this.id = props.match.params.id
    this.state = {
      industry: {}
    };
  }
  componentDidMount() {
    IndustryAPI.get(this.id).then(results => this.setState({ industry: results }))
  }

render() {

  if (!this.state.industry) {
    return <div><Container></Container></div>
  }
  return (
    <div>
      <Container style={{textAlign:"left"}}>
        <Row>
        <Col className="mb-2" sm="8">
          <h2 className="title">{this.state.industry.industry_name}</h2>
          <p className="description">{this.state.industry.description}</p>
        </Col>
          <Col className="mb-2" sm="4">
          <h4 className="industry-code">Industry Code: {this.state.industry.industry_id}</h4>
            <Card>
            <ListGroup>
              <ListGroupItem>GDP: <Badge style={{float: "right"}} color="info">{this.state.industry.gdp}</Badge></ListGroupItem>
              <ListGroupItem># of Firms:<Badge style={{float: "right"}} color="info" pill>{this.state.industry.num_of_firms}</Badge></ListGroupItem>
              <ListGroupItem>Top Business:<Badge style={{float: "right"}} color="amber darken-2" pill>{this.state.industry.top_scoring_business_name}</Badge></ListGroupItem>
            </ListGroup>
            </Card>
          </Col>


        </Row>
        <Row>
          <Col className="mt-2" sm="4">
            <Button onClick={() => this.props.history.goBack()}>Go Back</Button>
          </Col>
        </Row>
      </Container>
    </div>
  )
}
}

export default Industry
