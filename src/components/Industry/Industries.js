import React from 'react'
import { Switch, Route } from 'react-router-dom'
import IndustryList from './IndustryList'
import Industry from './Industry'

const Industries = () => (
  <Switch>
    <Route exact path='/industries' component={IndustryList}/>
    <Route path='/industries/:id' component={Industry}/>
  </Switch>
)


export default Industries
