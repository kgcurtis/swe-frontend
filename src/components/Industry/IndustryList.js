import React, { Component } from "react";
import IndustryAPI from '../../apis/industryAPI'
import { Container, Row, Col, Card, CardText, CardBody, CardTitle, Button, Pagination, PageLink, PageItem, ListGroup, ListGroupItem, Badge,Dropdown, DropdownToggle, DropdownMenu, DropdownItem  } from 'mdbreact';
import Highlighter from "react-highlight-words";


class IndustryList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      industries: [],
      pageNumber: 1,
      searchTerm: "",
      pages: [],
      startInd: 0,
      endInd: 12,
      sortFunction: function(a, b){return parseFloat(b.gdp) - parseFloat(a.gdp);},
      sortByName: "GDP High-Low",
      sizeFunction: function(b){return true; },
      sizeFilterName: "",
      globalSearch: this.props.searchTerm === undefined ? "" : this.props.searchTerm
    };

    this.filterList = this.filterList.bind(this);
    this.sortFunction = this.sortFunction.bind(this);
    this.gdpFilter = this.gdpFilter.bind(this);

  }

  componentDidMount() {
    var complete = [];
    if(this.state.globalSearch.length === 0){
    IndustryAPI.getAll().then(results => {
      this.setState({ industries: results, pages: Array.from(new Array(Math.floor(complete.length/12) + 1), (x,i) => i + 1) });
    });
  } else {
    IndustryAPI.searchAll(this.state.globalSearch).then(results => {
      this.setState({ industries: results, pages: Array.from(new Array(Math.floor(complete.length/12) + 1), (x,i) => i + 1) });
    });
  }
}
  getNext() {
    if(this.state.pageNumber < this.state.pages.length - 1){
      this.setState({
            startInd: this.state.startInd + 12,
            endInd: this.state.endInd + 12,
            pageNumber: this.state.pageNumber + 1
      })
    }
  }

  getPrev() {
      if(this.state.pageNumber > 1){
      this.setState({
            startInd: this.state.startInd - 12,
            endInd: this.state.endInd - 12,
            pageNumber: this.state.pageNumber - 1
      })
    }
  }

  getPage(pageNumber) {
    this.setState({
          startInd: (pageNumber - 1) * 12,
          endInd: (pageNumber * 12),
          pageNumber: pageNumber
      })
  }

  filterList(event) {
    this.setState({ searchTerm: event.target.value.toLowerCase() });
  }

  gdpFilter(size) {
    switch(size){
      case 0:
        this.setState({ sizeFilterName: "", sizeFunction: function(a){ return true;} });
        break;
      case 1:
        this.setState({ sizeFilterName: "Small", sizeFunction: function(a){ return parseFloat(a.gdp) < 500;} });
        break;
      case 2:
        this.setState({ sizeFilterName: "Medium", sizeFunction: function(a){ return parseFloat(a.gdp) > 500 && parseFloat(a.gdp) < 1000;} });
        break;
      case 3:
        this.setState({ sizeFilterName: "Large", sizeFunction: function(a){ return parseFloat(a.gdp) >= 1000;} });
        break;
      default:
          break;
    }
  }

  sortFunction(sortBy){

    switch(sortBy){
      case 1:
        this.setState({ sortByName: "Name A-Z", sortFunction: function(a, b){return a.industry_name.localeCompare(b.industry_name);} });
        break;
      case 2:
        this.setState({ sortByName: "Name Z-A", sortFunction: function(a, b){return b.industry_name.localeCompare(a.industry_name);} });
        break;
      case 3:
        this.setState({ sortByName: "GDP High-Low", sortFunction: function(a, b){return parseFloat(b.gdp) - parseFloat(a.gdp);} });
        break;
      case 4:
        this.setState({ sortByName: "GDP Low-High", sortFunction: function(a, b){return parseFloat(a.gdp) - parseFloat(b.gdp);} });
        break;
      case 5:
        this.setState({ sortByName: "# of Firms High-Low", sortFunction: function(a, b){return parseFloat(b.num_of_firms) - parseFloat(a.num_of_firms);} });
        break;
      case 6:
        this.setState({ sortByName: "# of Firms Low-High", sortFunction: function(a, b){return parseFloat(a.num_of_firms) - parseFloat(b.num_of_firms);} });
        break;
      default:
          break;
    }

  }

  render() {

    var regex = new RegExp(this.state.searchTerm.replace(/([()[{*+.$^\\|?])/g, '\\$&'), 'i');
     const industry = this.state.industries
    .filter(industry => regex.test(industry.industry_name) || regex.test(industry.gdp) || regex.test(industry.num_of_firms) || regex.test(industry.industry_id) || regex.test(industry.top_scoring_business_name))
    .filter(b => this.state.sizeFunction(b))
    .sort(this.state.sortFunction)
    .slice(this.state.startInd,this.state.endInd)
    .map(industry => (
        <Col sm="12" md="6" lg="4" key={industry.industry_id}>
          <Card style={{padding: "20px", margin: "0 0 30px 0", minHeight: "400px"}}>
            <CardBody>
              <CardTitle style={{fontSize: "18px", minHeight: "70px"}}><Highlighter searchWords={this.state.globalSearch.replace(/([()[{*+.$^\\|?])/g, '\\$&').split(" ")} textToHighlight={industry.industry_name}/></CardTitle>
              <ListGroup>
                <ListGroupItem>GDP: <Badge style={{float: "right"}} color="amber darken-2" pill><Highlighter searchWords={this.state.globalSearch.replace(/([()[{*+.$^\\|?])/g, '\\$&').split(" ")} textToHighlight={industry.gdp.toString()}/></Badge>  </ListGroupItem>
                <ListGroupItem># of Firms: <Badge style={{float: "right"}} color="amber darken-2" pill><Highlighter searchWords={this.state.globalSearch.replace(/([()[{*+.$^\\|?])/g, '\\$&').split(" ")} textToHighlight={industry.num_of_firms.toString()}/></Badge>  </ListGroupItem>

                  <ListGroupItem>Top Business: <Badge style={{maxWidth: "90px", float: "right"}} color="amber darken-2" pill><Highlighter searchWords={this.state.globalSearch.replace(/([()[{*+.$^\\|?])/g, '\\$&').split(" ")} textToHighlight={industry.top_scoring_business_name}/></Badge>  </ListGroupItem>

              </ListGroup>
              <CardText style={{padding:"10px 0 0 0"}}>Industry Code: <Highlighter searchWords={this.state.globalSearch.replace(/([()[{*+.$^\\|?])/g, '\\$&').split(" ")} textToHighlight={industry.industry_id}/></CardText>
            </CardBody>
            <Button color="amber" onClick={() => this.props.history.push(`/industries/${industry.industry_id}`)}>
              See More
          </Button>
          </Card>
        </Col>
      ));

      var num = Array.from(new Array(Math.floor(this.state.industries
     .filter(industry => regex.test(industry.industry_name))
     .filter(b => this.state.sizeFunction(b))
     .sort(this.state.sortFunction).length/12) + 1), (x,i) => i + 1)
      const pages = num.map(i => (
        <PageItem active={this.state.pageNumber === i} key={i}>
            <PageLink className="page-link" onClick={() => this.getPage(i)}>
               {i}
            </PageLink>
        </PageItem>
      ));

      return (
          <div>
            <Container>
            <div className="header">
              <h1 style={{fontSize:"50px", padding:"0 0 30px"}}>Industries</h1>
                {this.state.globalSearch.length === 0 &&
                 <form>
                   <fieldset className="form-group">
                   <input type="text" className="form-control form-control-md" placeholder="Search Industries..." onChange={this.filterList}/>
                   </fieldset>
                 </form>
               }
            </div>
            {this.state.globalSearch.length === 0 && <div>
              <Row>
                <Col>
                  <div>
                  <Dropdown size="sm" style={{float:'left'}}>
                    <DropdownToggle caret color="primary" className="force-scroll">
                    Sort By
                    </DropdownToggle>
                      <DropdownMenu>
                        <DropdownItem onClick={() => this.sortFunction(1)}><b>Name</b> A-Z</DropdownItem>
                        <DropdownItem onClick={() => this.sortFunction(2)}><b>Name</b> Z-A</DropdownItem>
                        <DropdownItem onClick={() => this.sortFunction(3)}><b>GDP</b> High-Low</DropdownItem>
                        <DropdownItem onClick={() => this.sortFunction(4)}><b>GDP</b> Low-High</DropdownItem>
                        <DropdownItem onClick={() => this.sortFunction(5)}><b># of Firms</b> High-Low</DropdownItem>
                        <DropdownItem onClick={() => this.sortFunction(6)}><b># of Firms</b> Low-High</DropdownItem>
                      </DropdownMenu>
                    </Dropdown>
                  <Dropdown size="sm" style={{float:'left'}}>
                      <DropdownToggle caret color="secondary">
                        Filter By Size
                      </DropdownToggle>
                        <DropdownMenu>
                            <DropdownItem onClick={()=> this.gdpFilter(0)}>None</DropdownItem>
                            <DropdownItem onClick={()=> this.gdpFilter(1)}>Small</DropdownItem>
                            <DropdownItem onClick={()=> this.gdpFilter(2)}>Medium</DropdownItem>
                            <DropdownItem onClick={()=> this.gdpFilter(3)}>Large</DropdownItem>
                        </DropdownMenu>
                      </Dropdown>
                  </div>
                </Col>
              </Row>
              <Row>
                <Col>
                  <div style={{float:'left', padding:'10px'}}>
                    <Badge color="grey" pill style={{margin: '10px', padding:'10px'}}>{this.state.sortByName}</Badge>
                    { this.state.sizeFilterName.length > 0 && <Badge color="grey" pill style={{margin: '10px', padding:'10px'}}>{this.state.sizeFilterName}</Badge> }
                  </div>
                </Col>
              </Row>
            </div> }
            <br/>
            <Row>
              { industry }
            </Row>
            <Row>
              <Col sm="12">
                <div style={{margin:"auto", width: "150px"}}>
                  <Pagination className="pg-teal">
                      <PageItem>
                          <PageLink className="page-link" aria-label="Previous" onClick={() => this.getPrev()}>&laquo;</PageLink>
                      </PageItem>
                      {pages}
                      <PageItem>
                          <PageLink className="page-link" aria-label="Next" onClick={() => this.getNext()}>&raquo;</PageLink>
                      </PageItem>
                  </Pagination>
                </div>
              </Col>
            </Row>
            </Container>
          </div>
      )
    }
  }
export default IndustryList
