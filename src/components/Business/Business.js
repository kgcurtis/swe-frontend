import React, { Component } from "react";
import BusinessAPI from '../../apis/businessAPI'
import { Container, Button, Card, CardImage, Col, Row, Badge, ListGroup, ListGroupItem } from 'mdbreact';
import MapContainer from '../MapContainer'
class Business extends Component {
  constructor(props) {
    super(props);
    this.id = props.match.params.id
    this.state = {
      business: null
    };
  }

  componentDidMount() {
    const res = BusinessAPI.get(this.id)
    res.then(results => this.setState({ business: results }))
  }

  render() {
    if (!this.state.business) {
      return <div><Container></Container></div>
    }
    return (
    <div className="business">
      <Container style={{textAlign:"left"}}>
        <Row>
        <Col className="mb-2" sm="7">
          <h1 className="title">{this.state.business.name}</h1>
          <h4 className="industry">{this.state.business.industry_name}
            <Button size="sm" onClick={() => this.props.history.push(`/industries/${this.state.business.industry_id}`)}>See More</Button>
          </h4>
          <h4 className="address">{this.state.business.address}</h4>
          <Button color="info" onClick={() => this.props.history.push(`/states/${this.state.business.state}`)}>{this.state.business.state}</Button>
          <div>
          <MapContainer lat={this.state.business.latitude} lng={this.state.business.longitude} zoom={17}/>
          </div>
          </Col>
          <Col className="mb-2" sm="5">
            <Card style={{height: 200, alignItems: 'center', justifyContent: 'center', padding: 20}}>
            <CardImage style={{margin: "auto"}}className="img-fluid" src={this.state.business.logo} />
            </Card>
            <br></br>
            <Card>
              <ListGroup>
                <ListGroupItem>BBB Rating:<Badge style={{float: "right"}} color="info">{this.state.business.bbb_rating}</Badge></ListGroupItem>
                <ListGroupItem>B-Corp Score:<Badge style={{float: "right"}} color="amber darken-2" pill>{this.state.business.overall_score}</Badge></ListGroupItem>
                <ListGroupItem><span className="item-tab"> Governance Score:<Badge style={{float: "right"}} color="teal" pill>{this.state.business.governance_score}</Badge></span></ListGroupItem>
                <ListGroupItem><span className="item-tab"> Workers Score:<Badge style={{float: "right"}} color="teal" pill>{this.state.business.workers_score}</Badge></span></ListGroupItem>
                <ListGroupItem><span className="item-tab"> Community Score:<Badge style={{float: "right"}} color="teal" pill>{this.state.business.community_score}</Badge></span></ListGroupItem>
                <ListGroupItem><span className="item-tab"> Environment Score:<Badge style={{float: "right"}} color="teal" pill>{this.state.business.environment_score}</Badge></span></ListGroupItem>
                <ListGroupItem><span className="item-tab"> Customers Score:<Badge style={{float: "right"}} color="teal" pill>{this.state.business.customers_score}</Badge></span></ListGroupItem>
                <ListGroupItem><span className="item-tab"> Operations Score:<Badge style={{float: "right"}} color="teal" pill>{this.state.business.operations_score}</Badge></span></ListGroupItem>
              </ListGroup>
            </Card>
          </Col>
        </Row>

        <Row>
          <Col className="mt-2" sm="4">
            <Button onClick={() => this.props.history.goBack()}>Go Back</Button>
          </Col>
        </Row>
      </Container>
    </div>
  );
}
}

export default Business
