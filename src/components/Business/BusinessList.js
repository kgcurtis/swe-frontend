import React, { Component } from "react";
import { withRouter } from 'react-router-dom'
import BusinessAPI from '../../apis/businessAPI'
import { Container, Row, Col, Card, CardText, CardBody, CardTitle, Button, ListGroup, ListGroupItem, Badge, Pagination, PageLink, PageItem, Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'mdbreact';
import Highlighter from "react-highlight-words";



class BusinessList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      businesses: [],
      stateAbbrs: ['AL','AK','AZ','AR','CA','CO','CT','DE','FL','GA','HI','ID','IL','IN','IA','KS','KY','LA','ME','MD','MA','MI','MN','MS',
      'MO','MT','NE','NV','NH','NJ','NM','NY','NC','ND','OH','OK','OR','PA','RI','SC','SD','TN','TX','UT','VT','VA','WA','WV','WI','WY'],
      industryNames: ['Professional Scientific and Technical Services','Administrative and Support and Waste Management and Remediation Services','Transportation and Warehousing','Manufacturing','Construction','Health Care and Social Assistance','Arts Entertainment and Recreation','Accommodation and Food Services','Wholesale Trade','Retail Trade','Information','Finance and Insurance','Educational Services','Other Services (except Public Administration)','Real Estate and Rental and Leasing'],
      searchTerm: "",
      pageNumber: 1,
      pages: [],
      startInd: 0,
      endInd: 12,
      stateFilter: "",
      industryFilter: "",
      sortFunction: function(a, b){return parseFloat(b.overall_score) - parseFloat(a.overall_score);},
      sortByName: "B-Corp Score High-Low",
      globalSearch: this.props.searchTerm === undefined ? "" : this.props.searchTerm
    };

    this.filterList = this.filterList.bind(this);
    this.sortFunction = this.sortFunction.bind(this);

  }

  componentDidMount() {
    var complete = [];
    if(this.state.globalSearch.length === 0){
      BusinessAPI.all().then(results => {
        this.setState({ businesses: results, pages: Array.from(new Array(Math.floor(complete.length/12) + 1), (x,i) => i + 1) });
      });
    } else {
      BusinessAPI.searchAll(this.state.globalSearch).then(results => {
        this.setState({ businesses: results, pages: Array.from(new Array(Math.floor(complete.length/12) + 1), (x,i) => i + 1) });
    });
  }
}

  getNext() {
    if(this.state.pageNumber < this.state.pages.length - 1){
      this.setState({
            startInd: this.state.startInd + 12,
            endInd: this.state.endInd + 12,
            pageNumber: this.state.pageNumber + 1
      })
    }
  }

  getPrev() {
      if(this.state.pageNumber > 1){
      this.setState({
            startInd: this.state.startInd - 12,
            endInd: this.state.endInd - 12,
            pageNumber: this.state.pageNumber - 1
      })
    }
  }

  getPage(pageNumber) {
    this.setState({
          startInd: (pageNumber - 1) * 12,
          endInd: (pageNumber * 12),
          pageNumber: pageNumber
      })
  }

  filterList(event) {
    this.setState({ searchTerm: event.target.value.toLowerCase() });
  }

  stateFilter(stateAbbr) {
      this.setState({ stateFilter: stateAbbr });
  }

  industryFilter(industry_name){
    this.setState({ industryFilter: industry_name });
  }

  sortFunction(sortAbbr){

    let grades = ["A+", "A", "A-", "B+", "B", "B-", "C+", "C", "C-", "D+", "F"]

    switch(sortAbbr){
      case 1:
        this.setState({ sortByName: "Name A-Z", sortFunction: function(a, b){return a.name.localeCompare(b.name);} });
        break;
      case 2:
        this.setState({ sortByName: "Name Z-A", sortFunction: function(a, b){return b.name.localeCompare(a.name);} });
        break;
      case 3:
        this.setState({ sortByName: "BBB Rating High-Low", sortFunction: function(a, b){ return grades.indexOf(a.bbb_rating) - grades.indexOf(b.bbb_rating);} });
        break;
      case 4:
        this.setState({ sortByName: "BBB Rating Low-High", sortFunction: function(a, b){ return grades.indexOf(b.bbb_rating) - grades.indexOf(a.bbb_rating);} });
        break;
      case 5:
        this.setState({ sortByName: "B-Corp Score High-Low", sortFunction: function(a, b){return parseFloat(b.overall_score) - parseFloat(a.overall_score);} });
        break;
      case 6:
        this.setState({ sortByName: "B-Corp Score Low-High", sortFunction: function(a, b){return parseFloat(a.overall_score) - parseFloat(b.overall_score);} });
        break;
      default:
        break;
    }

  }


  render() {
    console.log(this.state.globalSearch.replace(/([()[{*+.$^\\|?])/g, '\\$&').split(" "));
    var regex = new RegExp(this.state.searchTerm.replace(/([()[{*+.$^\\|?])/g, '\\$&'), 'i');
    var stateRegex = new RegExp(this.state.stateFilter, 'i');
    var industryRegex = new RegExp(this.state.industryFilter, 'i');

     const business = this.state.businesses
    .filter(b => regex.test(b.name) || regex.test(b.bbb_rating) || regex.test(b.overall_score) || regex.test(b.industry_name) || regex.test(b.website) || regex.test(b.state))
    .filter(b => stateRegex.test(b.state))
    .filter(b => industryRegex.test(b.industry_name))
    .sort(this.state.sortFunction)
    .slice(this.state.startInd,this.state.endInd)
    .map(b => (
      <Col className="business" sm="12" md="6" lg="4" key={b.business_id}>
          <Card style={{padding: "20px", margin: "0 0 30px 0", minHeight: "450px"}}>
            <CardBody>
              <CardTitle style={{fontSize: "22px", minHeight: "50px"}}><Highlighter searchWords={this.state.globalSearch.replace(/([()[{*+.$^\\|?])/g, '\\$&').split(" ")} textToHighlight={b.name}/></CardTitle>
              <CardText><Highlighter searchWords={this.state.globalSearch.replace(/([()[{*+.$^\\|?])/g, '\\$&').split(" ")} textToHighlight={b.industry_name}/></CardText>
              <ListGroup>
                <ListGroupItem>State:<Badge style={{float: "right"}} color="green darken-2"><Highlighter searchWords={this.state.globalSearch.replace(/([()[{*+.$^\\|?])/g, '\\$&').split(" ")} textToHighlight={b.state}/></Badge></ListGroupItem>
                <ListGroupItem>BBB Rating:<Badge style={{float: "right"}} color="green darken-2"><Highlighter searchWords={this.state.globalSearch.replace(/([()[{*+.$^\\|?])/g, '\\$&').split(" ")} textToHighlight={b.bbb_rating}/></Badge></ListGroupItem>
                <ListGroupItem>B-Corp Score:<Badge style={{float: "right"}} color="green darken-2"><Highlighter searchWords={this.state.globalSearch.replace(/([()[{*+.$^\\|?])/g, '\\$&').split(" ")} textToHighlight={b.overall_score.toString()}/></Badge></ListGroupItem>
              </ListGroup>
              <CardText style={{padding:"10px 0 0 0"}}><Highlighter searchWords={this.state.globalSearch.replace(/([()[{*+.$^\\|?])/g, '\\$&').split(" ")} textToHighlight={b.website}/></CardText>
              </CardBody>
              <Button color="dark-green" onClick={() => this.props.history.push(`/businesses/${b.business_id}`)}>
                See More
              </Button>
          </Card>
        </Col>
      ));
      var num = Array.from(new Array(Math.floor(this.state.businesses
     .filter(b => regex.test(b.name))
     .filter(b => stateRegex.test(b.state))
     .filter(b => industryRegex.test(b.industry_name))
     .sort(this.state.sortFunction).length/12) + 1), (x,i) => i + 1)
      const pages = num.map(i => (
        <PageItem active={this.state.pageNumber === i} key={i}>
            <PageLink className="page-link" onClick={() => this.getPage(i)}>
               {i}
            </PageLink>
        </PageItem>
      ));

      const stateDropdown = this.state.stateAbbrs.map(abbr => (
        <DropdownItem key={abbr} onClick={()=> this.stateFilter(abbr)}>{abbr}</DropdownItem>
      ));
      const industryDropdown = this.state.industryNames.map(ind => (
        <DropdownItem key={ind} onClick={()=> this.industryFilter(ind)}>{ind}</DropdownItem>
      ));

      const searchSortFilter = (
        <div>
        <form>
          <fieldset className="form-group">
          <input type="text" className="form-control form-control-md" placeholder="Search Businesses..." onChange={this.filterList}/>
          </fieldset>
        </form>
        <Row>
        <Col>
          <div>
            <Dropdown size="sm" style={{float:'left'}}>
            <DropdownToggle caret color="primary" className="force-scroll">
            Sort By
            </DropdownToggle>
              <DropdownMenu>
                <DropdownItem onClick={() => this.sortFunction(1)}><b>Name</b> A-Z</DropdownItem>
                <DropdownItem onClick={() => this.sortFunction(2)}><b>Name</b> Z-A</DropdownItem>
                <DropdownItem onClick={() => this.sortFunction(3)}><b>BBB Rating</b> High-Low</DropdownItem>
                <DropdownItem onClick={() => this.sortFunction(4)}><b>BBB Rating</b> Low-High</DropdownItem>
                <DropdownItem onClick={() => this.sortFunction(5)}><b>B-Corp Score</b> High-Low</DropdownItem>
                <DropdownItem onClick={() => this.sortFunction(6)}><b>B-Corp Score</b> Low-High</DropdownItem>
              </DropdownMenu>
            </Dropdown>
            <Dropdown size="sm" style={{float:'left'}}>
              <DropdownToggle caret color="secondary">
              Filter By State
              </DropdownToggle>
                <DropdownMenu>
                    <DropdownItem onClick={()=> this.stateFilter("")}>None</DropdownItem>
                  { stateDropdown }
                </DropdownMenu>
              </Dropdown>
              <Dropdown size="sm" style={{float:'left'}}>
                <DropdownToggle caret color="secondary">
                Filter By Industry
                </DropdownToggle>
                  <DropdownMenu>
                  <DropdownItem onClick={()=> this.industryFilter("")}>None</DropdownItem>
                    { industryDropdown }
                  </DropdownMenu>
                </Dropdown>
                </div>
          </Col>
        </Row>
      <div>
        <Row>
          <Col>
            <div style={{float:'left', padding:'10px'}}>
              <Badge color="grey" pill style={{margin: '10px', padding:'10px'}}>{this.state.sortByName}</Badge>
              { this.state.stateFilter.length > 0 && <Badge color="grey" pill style={{margin: '10px', padding:'10px'}}>{this.state.stateFilter}</Badge> }
              { this.state.industryFilter.length > 0 && <Badge color="grey" pill style={{margin: '10px', padding:'10px'}}>{this.state.industryFilter}</Badge> }
            </div>
          </Col>
        </Row>
      </div>
    </div>
      );

    return (
        <div className="business-list">
          <Container>
          <div className="header">
            <h1 style={{fontSize:"50px", padding:"0 0 30px"}}>Businesses</h1>
            {this.state.globalSearch.length === 0 && searchSortFilter }
          </div>
          <br/>
            <Row>
              { business }
            </Row>
            <Row>
            <Col sm="12">
            <div style={{margin:"auto", width: "275px"}}>
            <Pagination className="pg-secondary">
                <PageItem key={0}>
                    <PageLink className="page-link" aria-label="Previous" onClick={() => this.getPrev()}>&laquo;</PageLink>
                </PageItem>
                {pages}
                <PageItem key={8}>
                    <PageLink className="page-link" aria-label="Next" onClick={() => this.getNext()}>&raquo;</PageLink>
                </PageItem>
            </Pagination>
            </div>
            </Col>
            </Row>
          </Container>
        </div>
    )
  }
}

export default withRouter(BusinessList);
