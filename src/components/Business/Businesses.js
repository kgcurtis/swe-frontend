import React from 'react'
import { Switch, Route } from 'react-router-dom'
import BusinessList from './BusinessList'
import Business from './Business'

const Businesses = () => (
  <Switch>
    <Route exact path='/businesses' component={BusinessList}/>
    <Route path='/businesses/:id' component={Business}/>
  </Switch>
)

export default Businesses
