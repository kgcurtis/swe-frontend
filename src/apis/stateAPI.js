import Utils from './utils'

const StateAPI = {
  getAllQuery: function(params){
    return new Promise((resolve, reject) => {
      fetch('http://api.ethicalbusinesses.me/all_state/' + Utils.encodeParams(params))
      .then(dataWrappedByPromise => dataWrappedByPromise.json())
      .then(data => {
        resolve(data)
      })
    })
  },
  getAll: function() {
    return StateAPI.getAllQuery({});
  },
  searchAll: function(searchTerm) {
    return StateAPI.getAllQuery({search : searchTerm});
  },

  all: function() {
    return new Promise((resolve, reject) => {
      fetch('http://api.ethicalbusinesses.me/state/')
      .then(dataWrappedByPromise => dataWrappedByPromise.json())
      .then(data => {
        this.next = data.next
        this.prev = data.previous
        resolve(data)
     })
    })

   },
   getNext: function() {
     if(this.next){
       return new Promise((resolve, reject) => {
         fetch(this.next)
         .then(dataWrappedByPromise => dataWrappedByPromise.json())
         .then(data => {
           this.next = data.next
           this.prev = data.previous
           resolve(data)
        })
       })
     }
   },
   getPrev: function() {
     if(this.prev){
       return new Promise((resolve, reject) => {
         fetch(this.prev)
         .then(dataWrappedByPromise => dataWrappedByPromise.json())
         .then(data => {
           this.next = data.next
           this.prev = data.previous
           resolve(data)
        })
       })
     }
   },

   getPage: function(pageNumber) {
     return new Promise((resolve, reject) => {
         fetch('http://api.ethicalbusinesses.me/state/?page=' + pageNumber)
         .then(dataWrappedByPromise => dataWrappedByPromise.json())
         .then(data => {
           this.next = data.next
           this.prev = data.previous
           resolve(data)
        })
       })
   },
  get: function(id) {
    return new Promise((resolve, reject) => {
      fetch('http://api.ethicalbusinesses.me/state/' + id + '/')
      .then(data => {
        resolve(data.json())
     })
    })
  }
}

export default StateAPI
