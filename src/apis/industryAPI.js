import Utils from './utils';

const IndustryAPI = {

  getAllQuery: function(params){
    return new Promise((resolve, reject) => {
      fetch('http://api.ethicalbusinesses.me/all_industry/' + Utils.encodeParams(params))
      .then(dataWrappedByPromise => dataWrappedByPromise.json())
      .then(data => {
        resolve(data)
      })
    })
  },

  getAll: function() {
    return IndustryAPI.getAllQuery({});
  },
  searchAll: function(searchTerm) {
    return IndustryAPI.getAllQuery({ search : searchTerm });
  },

  all: function() {
    return new Promise((resolve, reject) => {
      fetch('http://api.ethicalbusinesses.me/industry/')
      .then(dataWrappedByPromise => dataWrappedByPromise.json())
      .then(data => {
        this.next = data.next
        this.prev = data.previous
        resolve(data)
     })
    })

   },
   getNext: function() {
     if(this.next){
       return new Promise((resolve, reject) => {
         fetch(this.next)
         .then(dataWrappedByPromise => dataWrappedByPromise.json())
         .then(data => {
           this.next = data.next
           this.prev = data.previous
           resolve(data)
        })
       })
     }
   },

   getPrev: function() {
     if(this.prev){
       return new Promise((resolve, reject) => {
         fetch(this.prev)
         .then(dataWrappedByPromise => dataWrappedByPromise.json())
         .then(data => {
           this.next = data.next
           this.prev = data.previous
           resolve(data)
        })
       })
     }
   },

   getPage: function(pageNumber) {
     return new Promise((resolve, reject) => {
         fetch('http://api.ethicalbusinesses.me/industry/?page=' + pageNumber)
         .then(dataWrappedByPromise => dataWrappedByPromise.json())
         .then(data => {
           this.next = data.next
           this.prev = data.previous
           resolve(data)
        })
       })
   },
  get: function(id) {
    return new Promise((resolve, reject) => {
      fetch('http://api.ethicalbusinesses.me/industry/' + id + '/')
      .then(data => {
        resolve(data.json())
     })
    })
  }
}

export default IndustryAPI
