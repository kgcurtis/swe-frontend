const Utils = {
  encodeParams: function(obj) {
    var str = "";
    for (var key in obj) {
      if(obj[key] !== undefined){
        if (str !== "") {
          str += "&";
        }
        str += key + "=" + encodeURIComponent(obj[key]);
      }
    }
    if(str !== "") {
      str = '?' + str;
    }
    return str;
  }
};

export default Utils;
