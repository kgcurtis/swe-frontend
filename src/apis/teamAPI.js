const TeamAPI = {

  all: function() {
     const promises = [fetch('https://gitlab.com/api/v4/projects/8511847/repository/commits?per_page=5000'),
               fetch('https://gitlab.com/api/v4/projects/8895032/repository/commits?per_page=5000'),
               fetch('https://gitlab.com/api/v4/projects/8511847/issues?per_page=5000'),
               fetch('https://gitlab.com/api/v4/projects/8895032/issues?per_page=5000')].map(p => p.then(res => res.json()))

   return Promise.all(promises)
  },

  getIssues: function(){
     return new Promise((resolve, reject) => {
        fetch('https://gitlab.com/api/v4/projects/8511847/issues')
            .then(dataWrappedByPromise => dataWrappedByPromise.json())
            .then(data => {
                resolve(data.length)
            })
    });
  }

}

export default TeamAPI;
