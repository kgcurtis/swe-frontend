import unittest
import os
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.common.exceptions import NoSuchElementException

# url = "http://localhost:3000"
url = "http://ethicalbusinesses.me"


class FrontendTesting(unittest.TestCase):

    def setUp(self):
        #Comment this out to run locally, uncomment when using as CI
        self.driver = webdriver.Remote(command_executor=os.environ['selenium_remote_url'],
                                       desired_capabilities=DesiredCapabilities.CHROME)

        #Need this to run locally
        # self.driver = webdriver.Chrome()

        self.driver.implicitly_wait(1)

    """
    The landing page should have navigation tab called 'Home'
    """

    def test_case_1(self):
        try:
            self.driver.get(url)
            el = self.driver.find_element_by_xpath("//div[contains(.,'Home')]")
        except NoSuchElementException as e:
            self.fail(e.msg)

    """
    The landing page should have navigation tab called 'Businesses'
    """

    def test_case_2(self):
        try:
            self.driver.get(url)
            el = self.driver.find_element_by_xpath(
                "//div[contains(.,'Businesses')]")
        except NoSuchElementException as e:
            self.fail(e.msg)

    """
    The landing page should have navigation tab called 'Industries'
    """

    def test_case_3(self):
        try:
            self.driver.get(url)
            el = self.driver.find_element_by_xpath(
                "//div[contains(.,'Industries')]")
        except NoSuchElementException as e:
            self.fail(e.msg)

    """
    The landing page should have navigation tab called 'States'
    """

    def test_case_4(self):
        try:
            self.driver.get(url)
            el = self.driver.find_element_by_xpath(
                "//div[contains(.,'States')]")
        except NoSuchElementException as e:
            self.fail(e.msg)

    # """
    # The landing page should have navigation tab called 'About'
    # """

    def test_case_5(self):
        try:
            self.driver.get(url)
            el = self.driver.find_element_by_xpath(
                "//div[contains(.,'About')]")
        except NoSuchElementException as e:
            self.fail(e.msg)

    # """
    # About page should have 5 cards, each for one developer
    # """

    def test_case_6(self):
        try:
            self.driver.get(url)
            el = self.driver.find_element_by_xpath(
                '//*[@id="root"]/div/nav/ul/li[4]/a')
            el.click()
            self.driver.implicitly_wait(1)
            cards = self.driver.find_elements_by_class_name('react-tabs__tab')
            self.assertEqual(5, len(cards))
        except NoSuchElementException as e:
            self.fail(e.msg)

    # """
    # About page should have a table for gitlab commits
    # """

    def test_case_7(self):
        try:
            self.driver.get(url)
            el = self.driver.find_element_by_xpath(
                '//*[@id="root"]/div/nav/ul/li[4]/a')
            el.click()
            self.driver.implicitly_wait(1)
            self.driver.find_elements_by_css_selector('table.table')
        except NoSuchElementException as e:
            self.fail(e.msg)

    # """
    # From businesses view, ensure there are 8 rows for information
    # """

    def test_case_8(self):
        try:
            self.driver.get('http://ethicalbusinesses.me/businesses/1000026721')
            rows = self.driver.find_elements_by_class_name('list-group-item')
            self.assertEqual(len(rows), 8)
        except NoSuchElementException as e:
            self.fail(e.msg)

    # """
    # From industries view, ensure there are 3 rows for information
    # """

    def test_case_9(self):
        try:
            self.driver.get('http://ethicalbusinesses.me/industries/31')
            rows = self.driver.find_elements_by_class_name('list-group-item')
            self.assertEqual(len(rows), 3)
        except NoSuchElementException as e:
            self.fail(e.msg)

    # """
    # From states view, ensure there are 4 rows for information
    # """

    def test_case_10(self):
        try:
            self.driver.get('http://ethicalbusinesses.me/states/CA')
            rows = self.driver.find_elements_by_class_name('list-group-item')
            self.assertEqual(len(rows), 4)
        except NoSuchElementException as e:
            self.fail(e.msg)

    # """
    # The home page should contain a search bar
    # """

    def test_case_11(self):
        try:
            self.driver.get(url)
            el = self.driver.find_element_by_xpath(
                '//*[@id="root"]/div/nav/ul/li[5]/form/button')
        except NoSuchElementException as e:
            self.fail(e.msg)

    """
    The businesses page should contain a 'Sort By' filter
    """

    def test_case_12(self):
        try:
            self.driver.get(url + '/businesses')
            el = self.driver.find_element_by_xpath(
                "//button[contains(.,'Sort By')]")
            el.click()
        except NoSuchElementException as e:
            self.fail(e.msg)

    """
    The businesses page should contain a 'Filter By State' filter
    """

    def test_case_13(self):
        try:
            self.driver.get(url + '/businesses')
            el = self.driver.find_element_by_xpath(
                "//button[contains(.,'Filter By State')]")
            el.click()
        except NoSuchElementException as e:
            self.fail(e.msg)

    """
    The businesses page should contain a 'Sort By Industry' filter
    """

    def test_case_14(self):
        try:
            self.driver.get(url + '/businesses')
            el = self.driver.find_element_by_xpath(
                "//button[contains(.,'Filter By Industry')]")
            el.click()
        except NoSuchElementException as e:
            self.fail(e.msg)

        """
    The industries page should contain a 'Sort By' filter
    """

    def test_case_15(self):
        try:
            self.driver.get(url + '/industries')
            el = self.driver.find_element_by_xpath(
                "//button[contains(.,'Sort By')]")
            el.click()
        except NoSuchElementException as e:
            self.fail(e.msg)

    """
    The industries page should contain a 'Filter By Size' filter
    """

    def test_case_16(self):
        try:
            self.driver.get(url + '/industries')
            el = self.driver.find_element_by_xpath(
                "//button[contains(.,'Filter By Size')]")
            el.click()
        except NoSuchElementException as e:
            self.fail(e.msg)

    """
    The states page should contain a 'Sort By' Filter
    """

    def test_case_17(self):
        try:
            self.driver.get(url + '/states')
            el = self.driver.find_element_by_xpath(
                "//button[contains(.,'Sort By')]")
            el.click()
        except NoSuchElementException as e:
            self.fail(e.msg)

    """
    The states page should contain a 'Filter By Largest Industry' Filter
    """

    def test_case_18(self):
        try:
            self.driver.get(url + '/states')
            el = self.driver.find_element_by_xpath(
                "//button[contains(.,'Filter By Largest Industry')]")
            el.click()
        except NoSuchElementException as e:
            self.fail(e.msg)

    """
    Navigate to businesses, sort A-Z, and then ensure '.org/advisors' is on page
    """

    def test_case_19(self):
        try:
            self.driver.get(url + '/businesses')
            el = self.driver.find_element_by_xpath(
                "//button[contains(.,'Sort By')]")
            el.click()
            el = self.driver.find_element_by_xpath(
                '//*[@id="root"]/div/div/main/div/div/div[1]/div/div/div/div[1]/div/button[1]')
            el.click()
            el=self.driver.find_element_by_xpath(
                "//div[contains(.,'.org/advisors')]")
        except NoSuchElementException as e:
            self.fail(e.msg)

    """
    Navigate to industries, sort A-Z, and then ensure 'Accommodation and Food Services' is on page
    """

    def test_case_20(self):
        try:
            self.driver.get(url + '/industries')
            el = self.driver.find_element_by_xpath(
                "//button[contains(.,'Sort By')]")
            el.click()
            el = self.driver.find_element_by_xpath(
                '//*[@id="root"]/div/div/main/div/div/div[2]/div[1]/div/div/div[1]/div/button[1]')
            el.click()
            el=self.driver.find_element_by_xpath(
                "//div[contains(.,'Accommodation and Food Services')]")
        except NoSuchElementException as e:
            self.fail(e.msg)



    def tearDown(self):
        self.driver.close()


if __name__ == "__main__":
    unittest.main()
