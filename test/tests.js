import React from 'react';
import Enzyme from 'enzyme';
import ReactSixteenAdapter from 'enzyme-adapter-react-16';
import {expect} from 'chai';
import { BrowserRouter, withRouter, MemoryRouter } from 'react-router-dom' // 4.0.0


import Business from '../src/components/Business/Business';
import BusinessList from '../src/components/Business/BusinessList';
import Industry from '../src/components/Industry/Industry';
import IndustryList from '../src/components/Industry/IndustryList';
import State from '../src/components/State/State';
import StateList from '../src/components/State/StateList';


import Home from '../src/components/Home';
import Main from '../src/components/Main';
import Header from '../src/components/Header';
import About from '../src/components/About';

Enzyme.configure({ adapter: new ReactSixteenAdapter() })

describe('<Home />', function () {
  it('should display the welcome message', function () {
    const wrapper = shallow(<Home />);
    expect(wrapper.contains('<h1>Welcome to the Ethical Businesses Website!</h1>'));
  });
});


describe('<Header />', function () {
  const wrapper = mount(<BrowserRouter><Header /></BrowserRouter>)

  it('should have a navbar class', function () {
    expect(wrapper.find('.navbar')).to.have.lengthOf(1);
  });

  it('should 4 nav items', function () {
    expect(wrapper.find('ul').children()).to.have.lengthOf(5);
  });
});


describe('Routes', function () {
  it('/ should display the Home component', function () {
    const wrapper = mount(
      <MemoryRouter initialEntries={[ '/' ]}>
        <Main/>
      </MemoryRouter>
    );
    expect(wrapper.find(Home)).to.have.lengthOf(1);
  });

  it('/businesses should display the business list component', function () {
    const wrapper = mount(
      <MemoryRouter initialEntries={[ '/businesses' ]}>
        <Main/>
      </MemoryRouter>
    );
    expect(wrapper.find(BusinessList)).to.have.lengthOf(1);
  });

  it('/businesses/:id should display a business component', function () {
    const wrapper = mount(
      <MemoryRouter initialEntries={[ '/businesses/1000026721' ]}>
        <Main/>
      </MemoryRouter>
    );
    expect(wrapper.find(Business)).to.have.lengthOf(1);
  });

  it('/industries should display the industry list component', function () {
    const wrapper = mount(
      <MemoryRouter initialEntries={[ '/industries' ]}>
        <Main/>
      </MemoryRouter>
    );
    expect(wrapper.find(IndustryList)).to.have.lengthOf(1);
  });

  it('/industries/:id should display an industry component', function () {
    const wrapper = mount(
      <MemoryRouter initialEntries={[ '/industries/54' ]}>
        <Main/>
      </MemoryRouter>
    );
    expect(wrapper.find(Industry)).to.have.lengthOf(1);
  });

  it('/states should display the state list component', function () {
    const wrapper = mount(
      <MemoryRouter initialEntries={[ '/states' ]}>
        <Main/>
      </MemoryRouter>
    );
    expect(wrapper.find(StateList)).to.have.lengthOf(1);
  });

  it('/states/:id should display a state component', function () {
    const wrapper = mount(
      <MemoryRouter initialEntries={[ '/states/CA' ]}>
        <Main/>
      </MemoryRouter>
    );
    expect(wrapper.find(State)).to.have.lengthOf(1);
  });
});

describe('<BusinessList />', function () {

  const wrapper = mount(
    <MemoryRouter initialEntries={[ '/businesses' ]}>
      <Main/>
    </MemoryRouter>
  );
  const component = wrapper.find(BusinessList);

  it('it should have at least 1 business component', function () {
    expect(component.find('t').length).to.be.gt(0);
  });

  it('it should have pagination', function () {
    expect(component.find('.pagination')).to.have.lengthOf(1);
  });

});

describe('<Business />', function () {

  it('should run <Business /> tests', (done) => {
  const wrapper = mount(
      <MemoryRouter initialEntries={[ '/businesses/1000026721' ]}>
        <Main/>
      </MemoryRouter>
    );
  const component = wrapper.find(Business);


  setImmediate(() => {
    it('it should have at title', function () {
      expect(component.find('.title')).to.have.lengthOf(1);
    });

    it('it should have an industry subtitle', function () {
      expect(component.find('.industry')).to.have.lengthOf(1);
    });

    it('it should have a list group of information', function () {
      expect(component.find('ul.list-group')).to.have.lengthOf(1);
    });

    it('it should have 3 list group items ', function () {
      expect(component.find('li.list-group-item')).to.have.lengthOf(8);
    });
    done();
  });
});


});

describe('<IndustryList />', function () {

  const wrapper = mount(
    <MemoryRouter initialEntries={[ '/industries' ]}>
      <Main/>
    </MemoryRouter>
  );
  const component = wrapper.find(IndustryList);

  it('it should have at least 1 industry component', function () {
    expect(component.find('t').length).to.be.gt(0);
  });

  it('it should have pagination', function () {
    expect(component.find('.pagination')).to.have.lengthOf(1);
  });

});

describe('<Industry />', function () {

  it('should run <Industry /> tests', (done) => {

  const wrapper = mount(
    <MemoryRouter initialEntries={[ '/industries/54' ]}>
      <Main/>
    </MemoryRouter>
  );
  const component = wrapper.find(Industry);

  setImmediate(() => {
  it('it should have at title', function () {
    expect(component.find('.title')).to.have.lengthOf(1);
  });

  it('it should have an industry subtitle', function () {
    expect(component.find('.description')).to.have.lengthOf(1);
  });

  it('it should have an industry subtitle', function () {
    expect(component.find('.industry-code')).to.have.lengthOf(1);
  });

  it('it should have a list group of information', function () {
    expect(component.find('ul.list-group')).to.have.lengthOf(1);
  });

  it('it should have 3 list group items', function () {
    expect(component.find('li.list-group-item')).to.have.lengthOf(3);
  });
  done();
});
});
});

describe('<StateList />', function () {


    const wrapper = mount(
      <MemoryRouter initialEntries={[ '/states' ]}>
        <Main/>
      </MemoryRouter>
    );
    const component = wrapper.find(StateList);




  it('it should have at least 1 state component', function () {
    expect(component.find('t').length).to.be.gt(0);
  });

  it('it should have pagination', function () {
    expect(component.find('.pagination')).to.have.lengthOf(1);
  });

});

describe('<States />', function () {

  it('should run <State /> tests', (done) => {
  const wrapper = mount(
    <MemoryRouter initialEntries={[ '/states/CA' ]}>
      <Main/>
    </MemoryRouter>
  );
  const component = wrapper.find(State);
  setImmediate(() => {

  it('it should have at title', function () {
    expect(component.find('.title')).to.have.lengthOf(1);
  });

  it('it should have a list group of information', function () {
    expect(component.find('ul.list-group')).to.have.lengthOf(1);
  });

  it('it should have 3 list group items ', function () {
    expect(component.find('li.list-group-item')).to.have.lengthOf(4);
  });
  done();
});
});
});

describe('<About />', function () {

  const wrapper = mount(
    <MemoryRouter initialEntries={[ '/about' ]}>
      <Main/>
    </MemoryRouter>
  );

  const component = wrapper.find(About);
  it('it should have a title', function () {
    expect(component.find('.title')).to.have.lengthOf(1);
  });

  it('it should have member tabs', function () {
    expect(component.find('ul.react-tabs__tab-list')).to.have.lengthOf(1);
  });

  it('it should have a stats table', function () {
    expect(component.find('table.table')).to.have.lengthOf(1);
  });

  it('the stats table should have 3 entries', function () {
    expect(component.find('l.stats-body').find('tr')).to.have.lengthOf(3);
  });

  it('it should have a list of tools', function () {
    expect(component.find('ul.tools')).to.have.lengthOf(1);
  });

  it('the list of tools should describe 9 tools', function () {
    expect(component.find('ul.tools').find('li.list-group-item')).to.have.lengthOf(9);
  });

  it('it should have a list of sources', function () {
    expect(component.find('ul.sources')).to.have.lengthOf(1);
  });

  it('the list of sources should mention 4 sources', function () {
    expect(component.find('ul.sources').find('li.list-group-item')).to.have.lengthOf(4);
  });

  it('it should have a list of links', function () {
    expect(component.find('div.links')).to.have.lengthOf(1);
  });

  it('the list of links should contain 6 links', function () {
    expect(component.find('div.links').find('p')).to.have.lengthOf(6);
  });
});

describe('Search Tests', function () {

  const wrapper = mount(
    <MemoryRouter initialEntries={[ '/search/a' ]}>
      <Main/>
    </MemoryRouter>
  );

  it('it should have at title', function () {
    expect(wrapper.find('h2')).to.have.lengthOf(1);
  });

  it('it should have a tabs for search results for each model', function () {
    expect(wrapper.find('.react-tabs')).to.have.lengthOf(2);
  });

  it('default tab should be BusinessList', function () {
    expect(wrapper.find(BusinessList)).to.have.lengthOf(1);
  });
});
